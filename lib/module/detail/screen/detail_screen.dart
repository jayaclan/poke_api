import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poke_api/module/detail/controller/detail_controller.dart';
import 'package:poke_api/module/detail/widget/About.dart';

import '../../../common/app_bar/AppBar.dart';
import '../widget/BaseStats.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetailController>(
      init: DetailController(),
      builder: (DetailController controller) {
        controller.tabController =
            TabController(length: controller.myTabs.tabs.length, vsync: this);
        return AppBarCustom(
          color: controller.color,
          title: '',
          child: Container(
            color: controller.color,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                              flex: 1,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    controller.name,
                                    style: const TextStyle(
                                        fontSize: 42,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  controller.detailPokemon != null
                                      ? Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding: const EdgeInsets.only(
                                                  left: 12,
                                                  right: 12,
                                                  top: 4,
                                                  bottom: 4),
                                              decoration: BoxDecoration(
                                                color: controller.color
                                                    .withBlue(20),
                                                borderRadius:
                                                    const BorderRadius.all(
                                                        Radius.circular(50)),
                                              ),
                                              child: Text(
                                                controller.detailPokemon
                                                    .types![0].type!.name!,
                                                style: const TextStyle(
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ),
                                            const SizedBox(
                                              width: 8,
                                            ),
                                            controller.detailPokemon.types!
                                                        .length >
                                                    1
                                                ? Container(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 12,
                                                            right: 12,
                                                            top: 4,
                                                            bottom: 4),
                                                    decoration: BoxDecoration(
                                                      color: controller.color
                                                          .withBlue(20),
                                                      borderRadius:
                                                          const BorderRadius
                                                                  .all(
                                                              Radius.circular(
                                                                  50)),
                                                    ),
                                                    child: Text(
                                                      controller
                                                          .detailPokemon!
                                                          .types![1]
                                                          .type!
                                                          .name!,
                                                      style: const TextStyle(
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w600),
                                                    ),
                                                  )
                                                : Row()
                                          ],
                                        )
                                      : Row()
                                ],
                              )),
                          Text(
                            "#${controller.index}",
                            style: const TextStyle(
                                fontSize: 42, fontWeight: FontWeight.w700),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Image.network(
                        controller.detailPokemon.sprites?.other?.officialArtwork
                                ?.frontDefault ??
                            "",
                        width: double.infinity,
                        height: 200,
                        fit: BoxFit.fitHeight,
                      ),
                    ],
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: ClipRRect(
                      borderRadius:  BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      child: Container(
                        color: Colors.white,
                        child: Scaffold(
                            appBar: PreferredSize(
                              preferredSize: controller.myTabs.preferredSize,
                              child: Material(
                                color: Colors.white,
                                child: TabBar(
                                  controller: controller.tabController,
                                  tabs: controller.myTabs.tabs,
                                  isScrollable: false,
                                  labelColor: Colors.black,
                                  unselectedLabelColor: Colors.grey,
                                  indicatorColor: Colors.blue,
                                  onTap: (index) {
                                    controller.tabIndex = index;
                                  },
                                  // indicator: const BoxDecoration(
                                  //     color: AppColors.blue,
                                  //     borderRadius: BorderRadius.only(
                                  //         bottomLeft: Radius.circular(10),
                                  //         bottomRight: Radius.circular(10))),
                                ),
                              ),
                            ),
                            body: Container(
                              height: double.infinity,
                              color: Colors.white,
                              child: TabBarView(
                                controller: controller.tabController,
                                children: [
                                  About(
                                      species: controller
                                          .detailPokemon.species!.name!,
                                      height:
                                          "${controller.detailPokemon.height}",
                                      weight:
                                          "${controller.detailPokemon.weight}",
                                      abilities:
                                          "${controller.detailPokemon.abilities![0].ability!.name}"),
                                  BaseStats(
                                    attack: controller
                                        .detailPokemon.stats![1].baseStat!,
                                    specialAttack: controller
                                        .detailPokemon.stats![3].baseStat!,
                                    defense: controller
                                        .detailPokemon.stats![2].baseStat!,
                                    specialDefense: controller
                                        .detailPokemon.stats![4].baseStat!,
                                    hp: controller
                                        .detailPokemon.stats![0].baseStat!,
                                    speed: controller
                                        .detailPokemon.stats![5].baseStat!,
                                  ),
                                ],
                              ),
                            )),
                      ),
                    ))
              ],
            ),
          ),
        );
      },
    );
  }
}
