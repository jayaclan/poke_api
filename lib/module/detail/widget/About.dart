import 'package:flutter/material.dart';

class About extends StatelessWidget {
  final String species;
  final String height;
  final String weight;
  final String abilities;

  About(
      {Key? key,
      required this.species,
      required this.height,
      required this.weight,
      required this.abilities})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all( 12),
      child: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                  flex: 1,
                  child: Text(
                    "Species",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey),
                  )),
              Expanded(
                  flex: 2,
                  child: Text(
                    species,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.black),
                  ))
            ],
          ),
          SizedBox(height: 12,),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                  flex: 1,
                  child: Text(
                    "Height",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey),
                  )),
              Expanded(
                  flex: 2,
                  child: Text(
                    height,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.black),
                  ))
            ],
          ),
          SizedBox(height: 12,),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                  flex: 1,
                  child: Text(
                    "Weight",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey),
                  )),
              Expanded(
                  flex: 2,
                  child: Text(
                    weight,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.black),
                  ))
            ],
          ),
          SizedBox(height: 12,),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                  flex: 1,
                  child: Text(
                    "Abilities",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey),
                  )),
              Expanded(
                  flex: 2,
                  child: Text(
                    abilities,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.black),
                  ))
            ],
          )
        ],
      ),
    );
  }
}
