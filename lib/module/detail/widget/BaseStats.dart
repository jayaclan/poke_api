import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../main/data/model/detailpokemon/DetailPokemon.dart';
import 'ProgressBar.dart';
import 'Stat.dart';

class BaseStats extends StatefulWidget {
  final int attack;
  final int specialAttack;
  final int defense;
  final int specialDefense;
  final int hp;
  final int speed;
  int get total => attack + specialAttack + defense + specialDefense + hp + speed;
   BaseStats({Key? key, required this.attack, required this.specialAttack, required this.defense, required this.specialDefense, required this.hp, required this.speed}) : super(key: key);

  @override
  State<BaseStats> createState() => _BaseStatsState();
}

class _BaseStatsState extends State<BaseStats> with TickerProviderStateMixin {
  late Animation<double> _progressAnimation;
  late AnimationController _progressController;
  @override
  void initState() {
    _progressController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
    );

    _progressAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
      curve: Curves.easeInOut,
      parent: _progressController,
    ));

    _progressController.forward();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12),
      child: Column(
        children: [
          Status(animation: _progressAnimation, label: 'Hp', value: widget.hp),
          SizedBox(height: 14),
          Status(animation: _progressAnimation, label: 'Atttack', value: widget.attack),
          SizedBox(height: 14),
          Status(animation: _progressAnimation, label: 'Defense', value: widget.defense),
          SizedBox(height: 14),
          Status(animation: _progressAnimation, label: 'Sp. Atk', value: widget.specialAttack),
          SizedBox(height: 14),
          Status(animation: _progressAnimation, label: 'Sp. Def', value: widget.specialDefense),
          SizedBox(height: 14),
          Status(animation: _progressAnimation, label: 'Speed', value: widget.speed),
          SizedBox(height: 14),
          Status(
            animation: _progressAnimation,
            label: 'Total',
            value: widget.total,
            progress: widget.total / 600,
          ),
        ],
      ),
    );
  }
}
