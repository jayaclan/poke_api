//Example of Repo
//You might not use it
import '../../../main/data/model/detailpokemon/DetailPokemon.dart';
import '../../../main/data/network/MainProvider.dart';

class DetailRepo {
  final _api = MainProvider();

  Future<dynamic> getDetail({required String baseurl}) async {
    try {
      var response = await _api.getDetail(baseurl);
      return DetailPokemon.fromJson(response.data);
    } catch (e) {
      rethrow;
    }
  }
}
