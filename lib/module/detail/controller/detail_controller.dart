import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poke_api/module/detail/data/repo/detail_repo.dart';

import '../../../core/styles/app_colors.dart';
import '../../main/data/model/detailpokemon/DetailPokemon.dart';

class DetailController extends GetxController with DetailRepo {
  late TabController tabController;
  DetailPokemon detailPokemon = DetailPokemon();
  String name = '';
  String index = '';
  dynamic argumentData = Get.arguments;
  Color color = AppColors.grey;


  TabBar get myTabs => const TabBar(
        tabs: [
          Tab(text: 'About'),
          Tab(text: 'Base Stats'),
        ],
      );

  late int tabIndex = 0;



  @override
  void onInit() {
    name = argumentData[0]['name'];
    detailPokemon = argumentData[1]['pokemon'];
    color = argumentData[2]['color'];
    index =  argumentData[3]['index'];
    super.onInit();
  }
}
