import 'dart:ui';

import 'package:get/get.dart';


import '../../../core/styles/app_colors.dart';
import '../data/model/AllData/AllDataPokemon.dart';
import '../data/model/detailpokemon/DetailPokemon.dart';
import '../data/repo/main_repo.dart';

class MainController extends GetxController with MainRepo {
  var allDataPokemon = AllDataPokemon();
  var detailPokemon = DetailPokemon();

  List<DetailPokemon> listPokemon = [];
  List<String> listURL = [];
  List<Color> listColor = [];

  bool loading = false;

  Future<void> getAllDataPokemon() async {
    loading = true;
    update();
    var response = await getAllData(
      limit: "88",
      offset: "0",
    );

    if (response is AllDataPokemon) {
      loading = false;
      allDataPokemon = response;
      allDataPokemon.results?.forEach((element) {
        listURL.add(element.url!);
      });

      await Future.forEach(listURL, (url) async{
        await getAllDetailPokemon(url);
      });
    } else {
      loading = false;
      try {} catch (e) {
        print("gagal");
      }
    }
    update();
  }


  Future<void> getAllDetailPokemon(String url) async {
    update();
    var response = await getDetail(
      baseurl: url,
    );

    if (response is DetailPokemon) {
      detailPokemon = response;
      listPokemon.add(detailPokemon);

      if(detailPokemon.types![0].type!.name! == "grass"){
        listColor.add(AppColors.lightGreen);
      }else if(detailPokemon.types![0].type!.name! == "fire"){
        listColor.add(AppColors.red);
      }else if(detailPokemon.types![0].type!.name! == "water" || detailPokemon.types![0].type!.name! == "ice"){
        listColor.add(AppColors.indigo);
      }
      else if(detailPokemon.types![0].type!.name! == "bug"){
        listColor.add(AppColors.brown);
      }
      else if(detailPokemon.types![0].type!.name! == "normal"){
        listColor.add(AppColors.tosca);
      }
      else if(detailPokemon.types![0].type!.name! == "ground"){
        listColor.add(AppColors.darkBrown);
      }
      else if(detailPokemon.types![0].type!.name! == "poison"){
        listColor.add(AppColors.purple);
      }else if(detailPokemon.types![0].type!.name! == "electric"){
        listColor.add(AppColors.lightYellow);
      } else if(detailPokemon.types![0].type!.name! == "fairy"){
        listColor.add(AppColors.pink);
      }else if(detailPokemon.types![0].type!.name! == "fighting"){
        listColor.add(AppColors.semiGrey);
      }
      else if(detailPokemon.types![0].type!.name! == "psychic" || detailPokemon.types![0].type!.name! == "dark"){
        listColor.add(AppColors.darkGrey);
      }
      else if(detailPokemon.types![0].type!.name! == "rock"){
        listColor.add(AppColors.brown);
      } else if(detailPokemon.types![0].type!.name! == "ghost"){
        listColor.add(AppColors.black);
      }else if(detailPokemon.types![0].type!.name! == "dragon"){
        listColor.add(AppColors.lightBlue);
      }
      else if(detailPokemon.types![0].type!.name! == "steel"){
        listColor.add(AppColors.lightCyan);
      }else{
        listColor.add(AppColors.white);
      }
    } else {
      try {} catch (e) {
        print("gagal");
      }
    }
    update();
  }

  @override
  void onInit() {
    super.onInit();
    getAllDataPokemon();
  }
}
