import 'package:flutter/material.dart';

import '../data/model/detailpokemon/DetailPokemon.dart';

class ItemList extends StatelessWidget {
  final String name;
  final GestureTapCallback? onTap;
  final int index;
  final DetailPokemon detailPokemon;

  final Color color;

  ItemList({
    Key? key,
    this.onTap,
    required this.name,
    required this.index,
    required this.detailPokemon,
    required this.color,
  }) : super(key: key);

  String test = '';

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 200,
        decoration:  BoxDecoration(
          color: color,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Text(
                      name,
                      maxLines: 1,
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                    )),
                Text(
                  "#${index + 1}",
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
                ),
              ],
            ),
            SizedBox(
              height: 8,
            ),
            Expanded(
              flex: 1,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                      flex: 1,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          detailPokemon.types![0] != null
                              ? Container(
                                  padding: const EdgeInsets.only(
                                      left: 12, right: 12, top: 4, bottom: 4),
                                  decoration: BoxDecoration(
                                    color: color.withBlue(20),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(50)),
                                  ),
                                  child: Text(
                                    detailPokemon.types![0].type!.name!,
                                    style: const TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600),
                                  ),
                                )
                              : Row(),
                          const SizedBox(
                            height: 4,
                          ),
                          detailPokemon!.types!.length > 1
                              ? Container(
                                  padding: const EdgeInsets.only(
                                      left: 12, right: 12, top: 4, bottom: 4),
                                  decoration: BoxDecoration(
                                    color: color.withBlue(20),
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(50)),
                                  ),
                                  child: Text(
                                    detailPokemon.types![1].type!.name!,
                                    style: const TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600),
                                  ),
                                )
                              : Row()
                        ],
                      )),
                  Expanded(
                    flex: 1,
                    child: detailPokemon.sprites?.other?.officialArtwork
                                ?.frontDefault?.isNotEmpty ==
                            true
                        ? Image.network(
                            detailPokemon
                                .sprites!.other!.officialArtwork!.frontDefault!,
                            width: double.infinity,
                            height: double.infinity,
                            fit: BoxFit.fitHeight,
                          )
                        : Column(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
