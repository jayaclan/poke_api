import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poke_api/module/detail/screen/detail_screen.dart';
import 'package:poke_api/module/main/controller/main_controller.dart';

import '../widget/ItemList.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainController>(
      init: MainController(),
      builder: (MainController controller) {
        return Scaffold(
          resizeToAvoidBottomInset: true,
          body: Container(
            color: Colors.white,
            child: SafeArea(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: controller.loading
                    ? Center(
                        child: Text(
                          "Loading.....",
                          style: TextStyle(
                              fontSize: 50, fontWeight: FontWeight.w700),
                        ),
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            "Pokedex ${controller.allDataPokemon.results!.length}",
                            style: TextStyle(
                                fontSize: 32, fontWeight: FontWeight.w700),
                          ),
                          controller.allDataPokemon != null &&
                                  controller
                                          .allDataPokemon.results?.isNotEmpty ==
                                      true
                              ? Flexible(
                                  flex: 1,
                                  child: GridView.builder(
                                      shrinkWrap: true,
                                      physics: ScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 2,
                                        childAspectRatio: 3/2,
                                        crossAxisSpacing: 10,
                                        mainAxisSpacing: 10,
                                      ),
                                      itemCount: controller.listPokemon.length,
                                      itemBuilder: (BuildContext ctx, index) {
                                        controller.getAllDetailPokemon(
                                            controller.allDataPokemon
                                                .results![index].url!);
                                        return ItemList(
                                          name: controller.allDataPokemon
                                              .results![index].name!,
                                          detailPokemon:
                                              controller.listPokemon[index],
                                          color: controller.listColor[index],
                                          index: index,
                                          onTap: () {
                                            Get.to(() => DetailScreen(),
                                                arguments: [
                                                  {
                                                    "name": controller
                                                        .allDataPokemon
                                                        .results![index]
                                                        .name!
                                                  },
                                                  {
                                                    "pokemon": controller
                                                        .listPokemon[index]
                                                  },
                                                  {
                                                    "color": controller
                                                        .listColor[index]
                                                  },
                                                  {"index": '${index + 1}'},
                                                ]);
                                            // Get.toNamed(RouteConstant.detail);
                                          },
                                        );
                                      }),
                                )
                              : Text(
                                  "No Data",
                                  style: TextStyle(
                                      fontSize: 32,
                                      fontWeight: FontWeight.w700),
                                ),
                        ],
                      ),
              ),
            ),
          ),
        );
      },
    );
  }
}
