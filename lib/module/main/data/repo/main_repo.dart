import '../network/MainProvider.dart';

import 'package:poke_api/module/main/data/model/AllData/AllDataPokemon.dart' as modelAllData;
import 'package:poke_api/module/main/data/model/detailpokemon/DetailPokemon.dart' as modelDetail;


class MainRepo {
  final _api = MainProvider();

  Future<dynamic> getAllData( {required String limit, required String offset}) async {
    try {
      var response = await _api.getAllData(limit, offset);
      return modelAllData.AllDataPokemon.fromJson(response.data);
    } catch (e) {
      rethrow;
    }
  }


  Future<dynamic> getDetail( {required String baseurl}) async {
    try {
      var response = await _api.getDetail(baseurl);
      return modelDetail.DetailPokemon.fromJson(response.data);
    } catch (e) {
      rethrow;
    }
  }

}