import '../../../../core/network/app_network.dart';

class MainProvider {
  Future<dynamic> getAllData(String limit, String offset) async {
    final response = await AppNetworkClient.get(
        path: "pokemon", data: {"limit": limit, "offset": offset});
    return response;
  }

  Future<dynamic> getDetail(String baseurl) async {
    final response = await AppNetworkClient.get( url: baseurl, path: '');
    return response;
  }
}
