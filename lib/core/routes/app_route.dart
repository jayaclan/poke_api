import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:poke_api/core/routes/route_constant.dart';
import 'package:poke_api/module/detail/screen/detail_screen.dart';
import 'package:poke_api/module/main/screen/main_screen.dart';


class AppRoute {
  static final all = [
    GetPage(name: RouteConstant.main, page: () => const MainScreen()),
    GetPage(name: RouteConstant.detail, page: () => const DetailScreen()),
  ];
}
