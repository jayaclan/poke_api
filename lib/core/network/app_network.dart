import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import '../constant/variable_constant.dart';

class AppNetworkClient {
  static String baseurl = VariableConstant.baseUrl;

  static BaseOptions options = BaseOptions(
    baseUrl: baseurl,
    connectTimeout: 3000,
  );

  static final Dio _dio = Dio(options)..interceptors.addAll([]);

  AppNetworkClient._();

  static final AppNetworkClient _instance = AppNetworkClient._();

  factory AppNetworkClient() {
    return _instance;
  }

  static Future<Response> get({
    Map<String, dynamic>? data,
    String? url,
    required String path,
  }) async {
    try {
      final Response res =
          await _dio.get((url ?? baseurl)+ path, queryParameters: data);


      return res;
    } on DioError catch (e) {
      _errorCatch(e);
      try {
        rethrow;
      } catch (e) {
        rethrow;
      }
    } catch (e) {
      throw "Something Went Wrong";
    }
  }

  static Future<Response> post({
    required Map<String, dynamic> data,
    String? url,
    Map<String, dynamic>? customHeader,
    required String path,
    FormData? form,
    // jsonMap for sending raw json to server
    Map<String, dynamic>? jsonMap,
  }) async {
    try {
      final res = await _dio.post((url ?? baseurl) + path,
          data: form ?? jsonMap ?? FormData.fromMap(data),
          options: Options(headers: customHeader));

      debugPrint("CALLING HEADERS " + res.requestOptions.headers.toString());
      debugPrint("CALIING METHOD " + res.requestOptions.method.toString());
      debugPrint("CALLING PATH " + res.requestOptions.path);
      debugPrint("CALLING DATA " + res.requestOptions.data.toString());

      return res;
    } on DioError catch (e) {
      _errorCatch(e);
      try {
        throw e;
      } catch (e) {
        rethrow;
      }
    } catch (e) {
      throw "Something Went Wrong";
    }
  }

  static void _errorCatch(DioError e) {
    try {
      if (e.response != null) {
        debugPrint("Error HEADERS ${e.requestOptions.headers}");
        debugPrint("Error METHOD ${e.requestOptions.method}");
        debugPrint("Error CALLING ${e.requestOptions.path}");
        debugPrint("Error DATA ${e.requestOptions.data}");
        debugPrint("Error QUERY ${e.requestOptions.queryParameters}");
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        debugPrint("CALLING ${e.requestOptions}");
        print(e.message);
      }
    } on Exception catch (e) {
      debugPrint("Exception $e");
    }
  }
}
