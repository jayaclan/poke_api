import 'package:flutter/material.dart';

class AppShadow {
  static List<BoxShadow> primary = [
    const BoxShadow(
        color: Color(0x15000000),
        offset: Offset(0, 2),
        blurRadius: 3,
        spreadRadius: 1),
  ];
  static List<BoxShadow> card = [
    const BoxShadow(
        color: Color(0xFFE0E0E0),
        offset: Offset(0, 5),
        blurRadius: 7,
        spreadRadius: 0),
  ];
}
