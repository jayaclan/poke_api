


import 'package:flutter/material.dart';

import '../../../core/styles/app_colors.dart';

class AppBarCustom extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Widget child;
  final Color color;


  AppBarCustom(
      {super.key,
        required this.title,
        required this.child,
         this.color = AppColors.blueSecond,});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          width: double.infinity,
          color: color,
          child: SafeArea(
            child: Scaffold(
              appBar: AppBar(
                backgroundColor: color,
                title:  Text(
                  title,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      color: AppColors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w700),
                ),
              ),
              body: Container(
                height: double.infinity,
                color: AppColors.background,
                child: child,
              ),
            ),
          ),
        ));
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => throw UnimplementedError();
}
